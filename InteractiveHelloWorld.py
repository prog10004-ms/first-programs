#Introduce the program and ask for the user's name
print(f'Hello. I am {__name__}. What is your name?')

#let "userName" be the value the user inputs
userName = input()

#say hello to the user using their name
print(f'Hello {userName}')